//
//  main.m
//  Learning0507
//
//  输出指定目录下的jpg文件
//
//  Created by  apple on 13-5-7.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{
    
    @autoreleasepool {
        //获取文件管理器
        NSFileManager * filemanager;
        filemanager = [NSFileManager defaultManager];
        
        //打开根目录
        NSString * home;
        //stringByExpandingTildeInPath 找到~代表的完整路径
        home = [@"~" stringByExpandingTildeInPath];
        NSLog(@"home is %@", home);
        
        //指定一个目录
        NSMutableString * dir = [NSMutableString stringWithCapacity:42];
        [dir appendString:home];
        [dir appendString:@"/Sites"];
        NSLog(@"dir is %@", dir);
        
        //将jpg文件的路径保存下来
        NSMutableArray * files = [NSMutableArray arrayWithCapacity:42];
        
        //enumeratorAtPath返回dir目录下的所有目录路径，返回值类型NSDirectoryEnumerator
        for(NSString * filename in [filemanager enumeratorAtPath:dir]){
            //pathExtension 获取扩展名（不包括.）
            if([[filename pathExtension] isEqualTo:@"jpg"]){
                [files addObject: filename];
            }
        }
        
        NSLog(@"pictures in home:");
        
        //输出jpg文件的路径,使用快速枚举方式输出
        for(NSString * filename in files){
            NSLog(@"%@", filename);
        }
        
    }
    return 0;
}